terraform {
  required_providers {
    openstack = {
      source                = "terraform-provider-openstack/openstack"
      version               = "1.51.1"
    }
  }
}

provider "openstack" {
  insecure    = true
  user_name   = "admin"
  password    = "WQRv9Ax2G=wNmJ^s"
  tenant_name = "Admin"
  auth_url    = "https://10.151.255.35:5000/v3"
}

resource "openstack_compute_instance_v2" "ins" {
  name            = var.insname
  image_name      = "golden-image-v1-13-17"
  flavor_name     = "INEEDFATCPU"
  key_pair        = "derrick"
  security_groups = ["default"]

  network {
    name = "server_vlan"
  }
}
